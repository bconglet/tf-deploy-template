# S3 Bucket

resource "aws_s3_bucket" "s3_bucket" {
  bucket        = "s3-bucket-${var.SHORT_ENVIRONMENT_NAME}"
  force_destroy = true

  tags = local.common_tags
}

# Output

output "s3_bucket" {
  value = aws_s3_bucket.s3_bucket.bucket
}
